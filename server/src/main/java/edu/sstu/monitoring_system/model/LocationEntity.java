package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "location")
public class LocationEntity {
    private Integer id;
    private String name;
    private Set<SensorEntity> sensors = new HashSet<>();

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    @JsonManagedReference
    public Set<SensorEntity> getSensors() {
        return sensors;
    }

    public void setSensors(Set<SensorEntity> sensors) {
        this.sensors = sensors;
    }
}
