package edu.sstu.monitoring_system.model.viewmodel;

import java.io.Serializable;
import java.util.List;

public class GraphicVM  implements Serializable {

    public String error;
    public List<ValueVM> values;
    public Percision percision;

    public enum Percision{VALUE, MINUTE, HOUR}

}