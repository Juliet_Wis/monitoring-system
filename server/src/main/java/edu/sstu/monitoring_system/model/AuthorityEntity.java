package edu.sstu.monitoring_system.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "authorities",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"username", "authority"}
        ))
public class AuthorityEntity implements Serializable {
    private UserEntity user;
    private String authority;

    public AuthorityEntity() {}

    public AuthorityEntity(UserEntity user, String authority) {
        this.user = user;
        this.authority = authority;
    }

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    public UserEntity getUsername() {
        return user;
    }

    public void setUsername(UserEntity user) {
        this.user = user;
    }

    @Id
    @Column(name = "authority", nullable = false, length = 30)
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}