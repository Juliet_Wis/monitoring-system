package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "value")
public class ValueEntity {
    private int id;
    private double value;
    private Date time;
    private SensorEntity sensor;
    private ParameterEntity parameter;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "value")

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

//    public String getTimeString() {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSS");
//       return df.format(time);
//    }

    @Column(name = "time")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensor_id", nullable = false)
    @JsonBackReference
    public SensorEntity getSensor() {
        return sensor;
    }

    public void setSensor(SensorEntity sensor) {
        this.sensor = sensor;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parameter_id", nullable = false)
    @JsonBackReference
    public ParameterEntity getParameter() {
        return parameter;
    }

    public void setParameter(ParameterEntity parameter) {
        this.parameter = parameter;
    }

    protected ValueEntity() {
    }

    public ValueEntity(double value, Date time, SensorEntity sensor, ParameterEntity parameter) {
        this.value = value;
        this.time = time;
        this.sensor = sensor;
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return String.format(
                "ValueEntity[id=%d, value="+ value + ", time='%s', sensor=%s]",
                id, time, sensor.getName());
    }
}
