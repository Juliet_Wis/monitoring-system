package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "parameter")
public class ParameterEntity implements Serializable{
    private int id;
    private String name;
    private UnitEntity unit;
    private Set<ValueEntity> values = new HashSet<>();
    private LimitEntity limit;
//    private DifferenceEntity difference;
//    private Set<DifferenceEntity> differences1 = new HashSet<>();
//    private Set<DifferenceEntity> differences2 = new HashSet<>();

    protected ParameterEntity() {
    }

    public ParameterEntity(String name, UnitEntity unit) {
        this.name = name;
        this.unit = unit;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id", nullable = false)
    @JsonBackReference
    public UnitEntity getUnit() {
        return unit;
    }

    public void setUnit(UnitEntity unit) {
        this.unit = unit;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "limit_id", nullable = false)
    @JsonBackReference
    public LimitEntity getLimit() {
        return limit;
    }

    public void setLimit(LimitEntity limit) {
        this.limit = limit;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parameter")
    @JsonManagedReference
    public Set<ValueEntity> getValues() {
        return values;
    }

    public void setValues(Set<ValueEntity> values) {
        this.values = values;
    }
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "difference_id")
//    @JsonBackReference
//    public DifferenceEntity getDifference() {
//        return difference;
//    }
//
//    public void setDifference(DifferenceEntity difference) {
//        this.difference = difference;
//    }

    @Override
    public String toString() {
        return String.format("ParameterEntity[id=%d, name='%s', unit='%s']", id, name, unit.getName());
    }
}
