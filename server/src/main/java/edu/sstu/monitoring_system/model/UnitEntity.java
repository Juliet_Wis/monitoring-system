package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "unit")
public class UnitEntity implements Serializable{
    private int id;
    private String name;
    private Set<ParameterEntity> parameters = new HashSet<>();

    protected UnitEntity() {
    }

    public UnitEntity(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "unit")
    @JsonManagedReference
    public Set<ParameterEntity> getParameters() {
        return parameters;
    }

    public void setParameters(Set<ParameterEntity> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return String.format(
                "UnitEntity[id=%d, name='%s']",
                id, name);
    }
}
