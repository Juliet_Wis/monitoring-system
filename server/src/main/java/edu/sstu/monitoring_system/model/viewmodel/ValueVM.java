package edu.sstu.monitoring_system.model.viewmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
public class ValueVM  implements Serializable {

    @Id
    @Column(name = "parameter_value_id")
    private int id;

    @Column(name = "sensor_id")
    public int sensorId;

    @Column(name = "value")
    public float value;

    @Column(name = "view_timestamp")
    public Timestamp time;

    public ValueVM() {
    }

    public ValueVM(int id, int sensorId, float value, Timestamp time) {
        this.id = id;
        this.sensorId = sensorId;
        this.value = value;
        this.time = time;
    }
}
