package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "message")
public class MessageEntity {
    private Integer id;
    private String title;
    private String text;
    private Set<LimitEntity> limits = new HashSet<>();
//    private Set<LimitEntity> differences = new HashSet<>();

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "message")
    @JsonManagedReference
    public Set<LimitEntity> getLimits() {
        return limits;
    }

    public void setLimits(Set<LimitEntity> limits) {
        this.limits = limits;
    }

//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "message")
//    @JsonManagedReference
//    public Set<LimitEntity> getDifferences() {
//        return differences;
//    }
//
//    public void setDifferences(Set<LimitEntity> differences) {
//        this.differences = differences;
//    }
}
