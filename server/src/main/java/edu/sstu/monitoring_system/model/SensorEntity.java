package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sensor")
public class SensorEntity implements Serializable{
    private int id;
    private String name;
    private SensorTypeEntity sensor_type;
    private String description;
    private LocationEntity location;
    private Set<ValueEntity> value = new HashSet<>();

    protected SensorEntity(){
    }

    public SensorEntity(String name, SensorTypeEntity sensor_type){
        this.name = name;
        this.sensor_type = sensor_type;
    }

    public SensorEntity(String name, SensorTypeEntity sensor_type, String description){
        this.name = name;
        this.sensor_type = sensor_type;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sensor")
    @JsonManagedReference
    public Set<ValueEntity> getValue() {
        return value;
    }

    public void setValue(Set<ValueEntity> value) {
        this.value = value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensor_type_id", nullable = false)
    @JsonBackReference
    public SensorTypeEntity getSensor_type() {
        return sensor_type;
    }


    public void setSensor_type(SensorTypeEntity sensor_type) {
        this.sensor_type = sensor_type;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id", nullable = false)
    @JsonBackReference
    public LocationEntity getLocation() {
        return location;
    }

    public void setLocation(LocationEntity location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return String.format(
                "SensorEntity[id=%d, name='%s', sensor_type=%s, description='%s']",
                id, name, sensor_type.getName(), description);
    }
}
