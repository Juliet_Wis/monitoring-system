package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

//@Entity
//@Table(name = "difference")
public class DifferenceEntity {
    private Integer id;
//    private ParameterEntity parameter1;
//    private ParameterEntity parameter2;
    private MessageEntity message;
    private Double difference;
    private Set<ParameterEntity> parameters = new HashSet<>();

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "difference")
    @JsonManagedReference
    public Set<ParameterEntity> getParameters() {
        return parameters;
    }

    public void setParameters(Set<ParameterEntity> parameters) {
        this.parameters = parameters;
    }

    //    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "parameter1_id")
//    public ParameterEntity getParameter1() {
//        return parameter1;
//    }
//
//    public void setParameter1(ParameterEntity parameter1) {
//        this.parameter1 = parameter1;
//    }
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "parameter2_id")
//    public ParameterEntity getParameter2() {
//        return parameter2;
//    }
//
//    public void setParameter2(ParameterEntity parameter2) {
//        this.parameter2 = parameter2;
//    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "message_id")
    @JsonBackReference
    public MessageEntity getMessage() {
        return message;
    }

    public void setMessage(MessageEntity message) {
        this.message = message;
    }

    @Column(name = "difference")
    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }
}