package edu.sstu.monitoring_system.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "sensor_type")
public class SensorTypeEntity implements Serializable{
    private int id;
    private String name;
    private Set<SensorEntity> sensors = new HashSet<>();
//    private Set<ParameterEntity> parameters = new HashSet<>();

    protected SensorTypeEntity() {
    }

    public SensorTypeEntity(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sensor_type")
    @JsonManagedReference
    public Set<SensorEntity> getSensors() {
        return sensors;
    }

    public void setSensors(Set<SensorEntity> sensors) {
        this.sensors = sensors;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(
                "Sensor_type[id=%d, name='%s']",
                id, name);
    }
}