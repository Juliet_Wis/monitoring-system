package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.LimitEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

//public interface ValueRepository extends CrudRepository<ValueEntity, Integer>{
//    @Query(value = "SELECT value FROM value v where v.id = :id",
//            nativeQuery=true)
//    double findValueById(@Param("id") Integer id);
//
//    @Query(value = "SELECT parameter_id FROM value v where v.id = :id",
//            nativeQuery=true)
//    Integer findParameterIdById(@Param("id") Integer id);
//}

public interface ValueRepository extends JpaRepository<ValueEntity, Integer>{
    List<ValueEntity> findAllBySensorId(int sensor_id);

    List<ValueEntity> findAllByParameterId(int i);
}