package edu.sstu.monitoring_system.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import edu.sstu.monitoring_system.model.viewmodel.ValueVM;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface ValueVMRepository extends JpaRepository<ValueVM, Integer> {

    @Query(nativeQuery = true, value = "select [id], [sensor_id], [value], [time] as [view_timestamp] \n" +
            "from value where sensor_id = :sensor_id and time between :from_time and :to_time")
    List<ValueVM> findValuesByParamIdInAndBetween(@Param("sensor_id") Integer sensor_id,
                                                  @Param("from_time") ZonedDateTime from, @Param("to_time") ZonedDateTime to);
    @Query(nativeQuery = true, value =
            ("select  min([id]) as parameter_value_id, [sensor_id], AVG([value]) as [value], \n" +
                    "  dateadd(minute, datediff(minute, 0, [time]), 0) as [view_timestamp] \n" +
                    "from [value] where [sensor_id] = :sensor_id\n" +
                    "and [time] between :from_time and :to_time \n" +
                    "group by dateadd(minute, datediff(minute, 0, [time]), 0), [sensor_id]\n" +
                    "order by [view_timestamp]"))
    List<ValueVM> findValuesByParamIdInAndBetweenAvgMinute(@Param("sensor_id") Integer sensor_id,
                                                           @Param("from_time") ZonedDateTime from,
                                                           @Param("to_time") ZonedDateTime to);

    @Query(nativeQuery = true, value =
            ("select  min([id]) as parameter_value_id, [sensor_id], AVG([value]) as [value], \n" +
                    "  dateadd(hour, datediff(hour, 0, [time]), 0) as [view_timestamp] \n" +
                    "from [value] where [sensor_id] = :sensor_id \n" +
                    "and [time] between :from_time and :to_time \n" +
                    "group by dateadd(hour, datediff(hour, 0, [time]), 0), [sensor_id]\n" +
                    "order by [view_timestamp]"))
    List<ValueVM> findValuesByParamIdInAndBetweenAvgHour(@Param("sensor_id") Integer sensor_id,
                                                         @Param("from_time") ZonedDateTime from, @Param("to_time") ZonedDateTime to);
}