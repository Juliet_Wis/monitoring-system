package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MessageEntity,Integer> {
}
