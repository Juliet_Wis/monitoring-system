package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserCrudRepository extends CrudRepository<UserEntity, String> {
    List<UserEntity> findByUsername(String username);
}