package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.UnitEntity;
import org.springframework.data.repository.CrudRepository;

public interface UnitRepository extends CrudRepository<UnitEntity, Integer> {
}
