package edu.sstu.monitoring_system.repo;


import edu.sstu.monitoring_system.model.ParameterEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ParameterRepository extends JpaRepository<ParameterEntity, Integer> {
    @Query(value = "SELECT limit_id FROM parameter p where p.id = :id",
            nativeQuery=true)
    Integer findLimitIdById(@Param("id") Integer id);
}

