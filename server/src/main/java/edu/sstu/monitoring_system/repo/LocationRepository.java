package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<LocationEntity, Integer> {
}
