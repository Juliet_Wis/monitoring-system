package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.SensorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SensorRepository extends JpaRepository<SensorEntity, Integer> {
    List<SensorEntity> findAllByLocationId(int location_id);

    @Query(value = "select * from sensor\n" +
            "where location_id=:id\n" +
            "ORDER BY sublocation, id",
            nativeQuery=true)
    List<SensorEntity> findSensorIdsByLocationId(@Param("id") Integer id);

    //List<SensorEntity> findAllSensorsByLocationIdIn(Iterable<Integer> ids);
}
