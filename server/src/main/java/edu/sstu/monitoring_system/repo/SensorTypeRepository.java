package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.SensorTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface SensorTypeRepository extends JpaRepository<SensorTypeEntity, Integer> {
}
