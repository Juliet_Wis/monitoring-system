package edu.sstu.monitoring_system.repo;

import edu.sstu.monitoring_system.model.LimitEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface LimitRepository extends CrudRepository<LimitEntity, Integer> {
    @Query(value = "SELECT min_value FROM limit l where l.id = :id",
            nativeQuery=true)
    Integer findMinValueById(@Param("id") Integer id);

    @Query(value = "SELECT max_value FROM limit l where l.id = :id",
            nativeQuery=true)
    Integer findMaxValueById(@Param("id") Integer id);
}
