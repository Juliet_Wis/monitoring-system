package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.LocationEntity;
import edu.sstu.monitoring_system.model.MessageEntity;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import edu.sstu.monitoring_system.repo.LocationRepository;
import edu.sstu.monitoring_system.repo.SensorRepository;
import edu.sstu.monitoring_system.repo.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/test3")
public class TestAjaxController {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private SensorRepository sensorRepository;
    @Autowired
    private ValueRepository valueRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView location(@PathVariable("id") int location_id, Model model) {
        LocationEntity location = locationRepository.getOne(location_id);
        ModelAndView mav = new ModelAndView("location");
        mav.addObject("id", location_id);
        mav.addObject("name", location.getName());
        return mav;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getPage(Model model) {
        return "test3";
    }

    @RequestMapping(value = "/getValues", method = RequestMethod.GET)
    List<Double> getSensorParamValues(@PathVariable("id") int location_id, Model model) {

        List<ValueEntity> vals = valueRepository.findAllBySensorId(1);
        List<Double> vlst = new ArrayList<>();
        for (ValueEntity v : vals) {
            vlst.add(v.getValue());
        }
        return vlst;
    }

//    @RequestMapping(value = "/{id}/initSensors", method = RequestMethod.GET)
//    public @ResponseBody
//    List<ValueEntity> initSensors(@PathVariable("id") int location_id, Model model) {
//        List<SensorEntity> sensors = sensorRepository.findAllByLocationId(location_id);
//        List<Integer> ids = sensors.stream()
//                .map(SensorEntity::getId).collect(Collectors.toList());
//
//        List<ValueEntity> values = new ArrayList<>();
//
//        for (Integer sensor_id : ids) {
//            values.addAll(em.createNativeQuery("SELECT TOP(1)" +
//                    "  sensor_id, +" +
//                    "  sensor.name as sensorName," +
//                    "  parameter_id," +
//                    "  parameter.name as parameterName," +
//                    "  unit.name as unitName," +
//                    "  limit.min_value," +
//                    "  limit.max_value, " +
//                    "  value.id as valueId," +
//                    "  value.value" +
//                    " FROM value" +
//                    "  INNER JOIN sensor ON value.sensor_id = sensor.id" +
//                    "  INNER JOIN parameter ON value.parameter_id = parameter.id" +
//                    "  INNER JOIN unit ON parameter.unit_id = unit.id" +
//                    "  INNER JOIN limit ON parameter.limit_id = limit.id" +
//                    " WHERE sensor_id = :id ORDER BY time DESC")
//                    .setParameter("id", sensor_id)
//                    .getResultList());
//        }
//        System.out.println(ids.size() + " " + values.size());
//        return values;
////     return sensorRepository.findAllByLocationId(location_id);
//    }
//
//    @RequestMapping(value = "/{id}/updateSensorValues", method = RequestMethod.GET)
//    public @ResponseBody
//    List<ValueEntity> getSensorParamValues(@PathVariable("id") int location_id, Model model) {
//        List<SensorEntity> sensors = sensorRepository.findAllByLocationId(location_id);
//        List<Integer> ids = sensors.stream()
//                .map(SensorEntity::getId).collect(Collectors.toList());
//
//        List<ValueEntity> values = new ArrayList<>();
//
//        for (Integer id : ids) {
//            values.addAll(em.createNativeQuery("SELECT TOP(1) value.id, parameter_id, value, time FROM value" +
//                    " WHERE sensor_id = :id ORDER BY time DESC")
//                    .setParameter("id", id)
//                    .getResultList());
//        }
//
//        return values;
//    }
}