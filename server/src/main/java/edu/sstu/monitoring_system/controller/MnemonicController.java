package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.ParameterEntity;
import edu.sstu.monitoring_system.repo.LimitRepository;
import edu.sstu.monitoring_system.repo.ParameterRepository;
import edu.sstu.monitoring_system.repo.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
/*
@Controller
public class MnemonicController {

    @Autowired
    ValueRepository valueRepository;

    @Autowired
    LimitRepository limitRepository;

    @Autowired
    ParameterRepository parameterRepository;

    double press1_value = 0;
    double press2_value = 0;
    double therm1_value = 71;
    double therm2_value = 75;
    double therm_delta = 0;
    double press_delta = 0;
    double press1_min =0;
    double press1_max =0;
    double press2_min =0;
    double press2_max =0;

    @RequestMapping("/mnemonic")
    public String getMnemonic(){
        return "mnemonicPage";
    }

    @RequestMapping("/mnemonic_sstu")
    public String getMnemonicSstu(){
        return "mnemonicSstuPage";
    }

    @RequestMapping("/boiler_room")
    public String getMnemonicBoilerRoom(){
        return "boilerRoomPage";
    }

    @RequestMapping("/boiler1")
    public String getBoiler1Page(Model model){
        initParameters();
        model.addAttribute("press1_value", 0.45);
        model.addAttribute("press2_value", 0.5);
        model.addAttribute("therm1_value", therm1_value);
        model.addAttribute("therm2_value", therm2_value);
        model.addAttribute("therm_delta", therm_delta);
        model.addAttribute("press_delta", 0.05);
//        model.addAttribute("therm1_color", "#464A4F");
//        model.addAttribute("therm2_color", "#464A4F");
        model.addAttribute("press1_color", getParameterColor(0.5, 0.3, 0.6));
        model.addAttribute("press2_color", getParameterColor(0.45, 0.3, 0.6));
        model.addAttribute("therm1_color", "#1d840e");
        model.addAttribute("therm2_color", "#1d840e");
        model.addAttribute("press_delta_color", getPressureDeltaColor(0.12));
        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(therm_delta));
        return ("boiler1Page");
    }

    @RequestMapping("/filter1")
    public String getFilter1Page(Model model){
        initParameters();
        model.addAttribute("press1_value", 0.6);
        model.addAttribute("press2_value", 0.39);
        model.addAttribute("therm1_value", 40);
        model.addAttribute("therm2_value", 40.7);
        model.addAttribute("therm_delta", 0.7);
        model.addAttribute("press_delta", 0.21);
        model.addAttribute("therm1_color", "#1d840e");
        model.addAttribute("therm2_color", "#1d840e");
        model.addAttribute("press1_color", getParameterColor(0.6, 0.3,0.6));
        model.addAttribute("press2_color", getParameterColor(0.39, 0.3, 0.6));
//        model.addAttribute("therm1_color", 0.2);
//        model.addAttribute("therm2_color", press_delta);
        model.addAttribute("press_delta_color", getPressureDeltaColor(0.2));
        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(0.7));
        return ("filter1Page");
    }

    @RequestMapping("/pump1")
    public String getPump1Page(Model model){
        initParameters();
        model.addAttribute("press1_value", 0.41);
        model.addAttribute("press2_value", 0.49);
        model.addAttribute("therm1_value", 38);
        model.addAttribute("therm2_value", 38.4);
        model.addAttribute("therm_delta", 0.4);
        model.addAttribute("press_delta", 0.08);
        model.addAttribute("therm1_color", "#1d840e");
        model.addAttribute("therm2_color", "#1d840e");
        model.addAttribute("press1_color", getParameterColor(0.41, 0.3, 0.6));
        model.addAttribute("press2_color", getParameterColor(0.49, 0.3, 0.6));
//        model.addAttribute("therm1_color", press_delta);
//        model.addAttribute("therm2_color", press_delta);
        model.addAttribute("press_delta_color", getPressureDeltaColor(0.08));
        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(0.4));
        return ("pump1Page");
    }

    @RequestMapping({"/br_enter1", "/br_out1", "/heat_excharger1", "/comb1"})
    public String getBrEnter1Page(Model model){
        initParameters();
        model.addAttribute("press1_value", 0.4);
        model.addAttribute("press2_value", 0.43);
        model.addAttribute("therm1_value", 37);
        model.addAttribute("therm2_value", 37.2);
        model.addAttribute("therm_delta", 0.2);
        model.addAttribute("press_delta", 0.03);
        model.addAttribute("therm1_color", "#464A4F");
        model.addAttribute("therm2_color", "#464A4F");
        model.addAttribute("press1_color", getParameterColor(0.4, 0.3, 0.6));
        model.addAttribute("press2_color", getParameterColor(0.43, 0.3, 0.6));
//        model.addAttribute("therm1_color", press_delta);
//        model.addAttribute("therm2_color", press_delta);
        model.addAttribute("press_delta_color", getPressureDeltaColor(0.03));
        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(0.2));
        return ("brEnter1Page");
    }

//    @RequestMapping("/br_out1")
//    public String getBrOut1Page(Model model){
//        initParameters();
//        model.addAttribute("press1_value", press1_value);
//        model.addAttribute("press2_value", press2_value);
//        model.addAttribute("therm1_value", therm1_value);
//        model.addAttribute("therm2_value", therm2_value);
//        model.addAttribute("therm_delta", therm_delta);
//        model.addAttribute("press_delta", press_delta);
//        model.addAttribute("therm1_color", "#464A4F");
//        model.addAttribute("therm2_color", "#464A4F");
//        model.addAttribute("press1_color", getParameterColor(press1_value, press1_min, press1_max));
//        model.addAttribute("press2_color", getParameterColor(press1_value, press2_min, press2_max));
//        model.addAttribute("therm1_color", "#1d840e");
//        model.addAttribute("therm2_color", "#1d840e");
//        model.addAttribute("press_delta_color", getPressureDeltaColor(press_delta));
//        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(therm_delta));
//        return ("brEnter1Page");
//    }

//    @RequestMapping("/heat_excharger1")
//    public String getExcharger1Page(Model model){
//        initParameters();
//        model.addAttribute("press1_value", press1_value);
//        model.addAttribute("press2_value", press2_value);
//        model.addAttribute("therm1_value", therm1_value);
//        model.addAttribute("therm2_value", therm2_value);
//        model.addAttribute("therm_delta", therm_delta);
//        model.addAttribute("press_delta", press_delta);
//        model.addAttribute("therm1_color", "#464A4F");
//        model.addAttribute("therm2_color", "#464A4F");
//        model.addAttribute("press1_color", getParameterColor(press1_value, press1_min, press1_max));
//        model.addAttribute("press2_color", getParameterColor(press1_value, press2_min, press2_max));
//        model.addAttribute("therm1_color", press_delta);
//        model.addAttribute("therm2_color", press_delta);
//        model.addAttribute("press_delta_color", getPressureDeltaColor(press_delta));
//        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(therm_delta));
//        return ("brEnter1Page");
//    }
//
//    @RequestMapping("/comb1")
//    public String getComb1Page(Model model){
//        initParameters();
//        model.addAttribute("press1_value", press1_value);
//        model.addAttribute("press2_value", press2_value);
//        model.addAttribute("therm1_value", therm1_value);
//        model.addAttribute("therm2_value", therm2_value);
//        model.addAttribute("therm_delta", therm_delta);
//        model.addAttribute("press_delta", press_delta);
//        model.addAttribute("therm1_color", "#464A4F");
//        model.addAttribute("therm2_color", "#464A4F");
//        model.addAttribute("press1_color", getParameterColor(press1_value, press1_min, press1_max));
//        model.addAttribute("press2_color", getParameterColor(press1_value, press2_min, press2_max));
//        model.addAttribute("therm1_color", press_delta);
//        model.addAttribute("therm2_color", press_delta);
//        model.addAttribute("press_delta_color", getPressureDeltaColor(press_delta));
//        model.addAttribute("therm_delta_color", getThemperatureDeltaColor(therm_delta));
//        return ("brEnter1Page");
//    }

    private void initParameters(){
        press1_value= valueRepository.findValueById(1);
        press2_value= valueRepository.findValueById(2);
        therm_delta = Math.abs(therm1_value - therm2_value);
        press_delta = Math.abs(press1_value - press2_value);
        press1_min = limitRepository.findMinValueById(parameterRepository.findLimitIdById(valueRepository.findParameterIdById(1)));
        press1_max = limitRepository.findMaxValueById(parameterRepository.findLimitIdById(valueRepository.findParameterIdById(1)));
        press2_min = limitRepository.findMinValueById(parameterRepository.findLimitIdById(valueRepository.findParameterIdById(2)));
        press2_max = limitRepository.findMaxValueById(parameterRepository.findLimitIdById(valueRepository.findParameterIdById(2)));
    }

    private String getParameterColor(double value, double min, double max){
        if (value <= max && value >= min)
//            grey
            return "#1d840e";
//        red
        else return "#B22222";
    }

    private String getThemperatureDeltaColor(double delta){
//        green
        if (delta <= 4) return "#1d840e";
        else return "#B22222";
    }
    private String getPressureDeltaColor(double delta){
        if (delta <= 0.1) return "#1d840e";
        else return "#B22222";
    }
}
*/