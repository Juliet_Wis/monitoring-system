package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.repo.ValueRepository;
import edu.sstu.monitoring_system.model.ValueEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Controller
public class IndexController {
    @Autowired
    ValueRepository valueRepository;
    ValueEntity value;

//    @RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
//    public String homePage(Model model) {
//        return "homePage";
//    }


    @RequestMapping(value = { "/contactus" }, method = RequestMethod.GET)
    public String contactusPage(Model model) {
        model.addAttribute("address", "Vietnam");
        model.addAttribute("phone", "...");
        model.addAttribute("email", "...");
        return "contactusPage";
    }

    @RequestMapping("/termometer")
    public String getTermometerPage(){
        return ("termometerPage");
    }

    @RequestMapping("/manometer")
    public String getManometerPage(Model model){
        double val = 0;
//        val = valueRepository.findValueById(1);

        model.addAttribute("man1_value", val);
        model.addAttribute("man2_value", 4);
        return ("manometerPage");
    }
}