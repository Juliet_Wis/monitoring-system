package edu.sstu.monitoring_system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test")
public class TestMnemonicController {
    @RequestMapping("/")
    public String getTestMnemonic(){
        return "testMnemonicPage";
    }

////    @PersistenceContext private EntityManager em;
////    @Autowired private SensorPointsRepository sensorPointsRepository;
////    @Autowired private SensorsRepository sensorsRepository;
////    @Autowired private ParametersRepository parametersRepository;
////    @Autowired private ValuesRepository valuesRepository;
//
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//    public ModelAndView sensorPoint(@PathVariable("id")int sensorpoint_id, Model model)
//    {
////        SensorPoint sensorPoint = sensorPointsRepository.getOne(sensorpoint_id);
//        ModelAndView mav = new ModelAndView("sensorpoint");
//        mav.addObject("id", sensorpoint_id);
////        mav.addObject("img_url", sensorPoint.getImgUrl());
////        mav.addObject("name", sensorPoint.getElectric_panel());
//        return mav;
//    }
//
//    @RequestMapping(value = "/{id}/initSensors", method = RequestMethod.GET)
//    public @ResponseBody
//    List<Sensor> initSensors(@PathVariable("id")int sensorpoint_id, Model model)
//    {
//        return sensorsRepository.findAllByPointId(sensorpoint_id);
//    }
//
//
//    @RequestMapping(value = "/{id}/updateSensorValues", method = RequestMethod.GET)
//    public @ResponseBody
//    List<Value> getSensorParamValues(@PathVariable("id")int sensorpoint_id, Model model) {
//        List<Parameter> params = findParametersBySensorPointId(sensorpoint_id);
//        List<Integer> ids = params.stream()
//                .map(Parameter::getId).collect(Collectors.toList());
//        // model.addAttribute("id", sensorpoint_id);
//        return em.createNativeQuery("SELECT TOP(:size) parameter_id, value, creation_timestamp FROM parameter_values" +
//                " WHERE parameter_id IN :ids ORDER BY creation_timestamp DESC")
//                .setParameter("size", params.size())
//                .setParameter("ids", ids)
//                .getResultList();
//    }
//
//
//    private List<Parameter> findParametersBySensorPointId(int sensorpoint_id){
//        List<Sensor>sensors = sensorsRepository.findAllByPointId(sensorpoint_id);
//        List<Integer> ids = sensors.stream()
//                .map(Sensor::getId).collect(Collectors.toList());
//        return parametersRepository.findAllParametersBySensorIdIn(ids);
//    }
}
