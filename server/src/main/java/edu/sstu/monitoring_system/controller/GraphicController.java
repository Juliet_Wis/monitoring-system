package edu.sstu.monitoring_system.controller;


import edu.sstu.monitoring_system.model.ParameterEntity;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import edu.sstu.monitoring_system.model.viewmodel.GraphicVM;
import edu.sstu.monitoring_system.repo.ParameterRepository;
import edu.sstu.monitoring_system.repo.SensorRepository;
import edu.sstu.monitoring_system.repo.ValueRepository;
import edu.sstu.monitoring_system.repo.ValueVMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;



@Controller
@RequestMapping(value = "/graphic")
public class GraphicController {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    ParameterRepository paramsRepository;

    @Autowired
    ValueRepository valuesRepository;

    @Autowired
    SensorRepository sensorRepository;

    @Autowired
    ValueVMRepository valuesVMRepository;

    private Integer sensor_id = 1;

    @RequestMapping(value = "/sensor-{id}", method = RequestMethod.GET)
    public String graphicPage(@PathVariable("id")int sensor_id, Model model){
        this.sensor_id = sensor_id;
        List<SensorEntity> sensors = new ArrayList<>();
                sensors.add(sensorRepository.getOne(sensor_id));
        model.addAttribute("sensors", sensors);
        return "graphicPage";
    }

    //Выбирает из базы значения измерений параметров в зависимости от временного промежутка
    @RequestMapping(value = "/sensor-{id}/getData", method = RequestMethod.POST)
    public @ResponseBody GraphicVM getGraphicData(//@PathVariable("id")int sensor_id,
                                                  @RequestParam(value = "paramIds[]")Integer[] sensor_ids,
                                                  @RequestParam(value = "from")long from,
                                                  @RequestParam(value = "to") long to, Model model){
        GraphicVM viewModel = new GraphicVM();
        ZonedDateTime fromTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneOffset.UTC);
        ZonedDateTime toTime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneOffset.UTC);

        long minutes = fromTime.until(toTime, ChronoUnit.MINUTES);
        long days = fromTime.until(toTime, ChronoUnit.DAYS);

        if(minutes <= 10){
            viewModel.values = valuesVMRepository.findValuesByParamIdInAndBetween(sensor_id, fromTime, toTime);
            viewModel.percision = GraphicVM.Percision.VALUE;
        }
        else if(minutes <= 60){
            viewModel.values = valuesVMRepository.findValuesByParamIdInAndBetweenAvgMinute(sensor_id, fromTime, toTime);
            viewModel.percision = GraphicVM.Percision.MINUTE;
        }
        else if(days < 7){
            viewModel.values = valuesVMRepository.findValuesByParamIdInAndBetweenAvgHour(sensor_id, fromTime, toTime);
            viewModel.percision = GraphicVM.Percision.HOUR;
        }
        else {
            viewModel.error = "Вывести данные можно за период, не превышающий недели.";
        }
        return viewModel;
    }

}
