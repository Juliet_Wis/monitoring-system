package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.LocationEntity;
import edu.sstu.monitoring_system.model.SensorTypeEntity;
import edu.sstu.monitoring_system.repo.LocationRepository;
import edu.sstu.monitoring_system.repo.SensorTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sensor")
public class AddSensorController {

    @Autowired
    SensorTypeRepository sensorTypeRepository;
    @Autowired
    LocationRepository locationRepository;

    @RequestMapping("/addSensor")
    public String addSensor(Model model) {
        List<String> types = getSensorTypes();
        for (String s:types
             ) {
            System.out.println("SENSOR TYPE - " + s);
        }
        model.addAttribute("sensorTypes", types);
        model.addAttribute("locations", getLocations());
        model.addAttribute("sensorId", 1);
        return "addSensor";
    }

    private List<String> getSensorTypes() {
        List<String> names = new ArrayList<>();
        List<SensorTypeEntity> sensorTypes = sensorTypeRepository.findAll();
        for (SensorTypeEntity st:sensorTypes) {
            names.add(st.getName());
        }
        return names;
    }

    private List<String> getLocations() {
        List<String> names = new ArrayList<>();
        List<LocationEntity> locations = locationRepository.findAll();
        for (LocationEntity loc:locations) {
            names.add(loc.getName());
        }
        return names;
    }


    @RequestMapping(value="/saveSensor", method = RequestMethod.POST)
    public void saveSensor(@ModelAttribute("sensorType") SensorTypeEntity sensorType){
        System.out.println(sensorType.getName());
    }
}
