package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.LocationEntity;
import edu.sstu.monitoring_system.model.MessageEntity;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import edu.sstu.monitoring_system.repo.LocationRepository;
import edu.sstu.monitoring_system.repo.MessageRepository;
import edu.sstu.monitoring_system.repo.SensorRepository;
import edu.sstu.monitoring_system.repo.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/location")
public class LocationController {
    @Autowired
    LocationRepository locationRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    ValueRepository valueRepository;
    @Autowired
    SensorRepository sensorRepository;
    @PersistenceContext
    EntityManager em;

    private int pressureParamId = 2;
    private int thempParamId = 4;

//    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//    public ModelAndView location(@PathVariable("id") int location_id, Model model) {
//        LocationEntity location = locationRepository.getOne(location_id);
//        ModelAndView mav = new ModelAndView("location");
//        mav.addObject("id", location_id);
//        mav.addObject("name", location.getName());
//        return mav;
//    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String showLocation(@PathVariable("id") int location_id, Model model) {
        LocationEntity location = locationRepository.getOne(location_id);
        model.addAttribute("locationId", location_id);
        model.addAttribute("locationName", location.getName());
        model.addAttribute("sensorId", 1);
        return "locationPage";
    }

    @RequestMapping(value = "/{id}/showMessage", method = RequestMethod.GET)
    public @ResponseBody
    List<MessageEntity> getSensorParamValues(@PathVariable("id") int location_id, Model model) {

        List<SensorEntity> sensors = sensorRepository.findAllByLocationId(location_id);
        List<MessageEntity> messages= new ArrayList<>();
        Integer sensorId;
        for (SensorEntity sensor : sensors) {
            sensorId = sensor.getId();
            String limitQuery = "select message.title, message.text from (select TOP(1) value, parameter_id from value\n" +
                    "  inner join sensor on value.sensor_id = sensor.id\n" +
                    "where sensor_id = :id ORDER BY time desc) t\n" +
                    "  INNER JOIN parameter on t.parameter_id = parameter.id\n" +
                    "  INNER JOIN limit on parameter.limit_id = limit.id\n" +
                    "  INNER JOIN message on limit.message_id = message.id\n" +
                    "where ((value < limit.min_value) or (value > limit.max_value))";

            messages.addAll(em.createNativeQuery(limitQuery).setParameter("id", sensorId).getResultList());
        }
        return messages;
    }

    @RequestMapping(value = "/{id}/init", method = RequestMethod.GET)
    public @ResponseBody
    List<ValueEntity> init(@PathVariable("id") int location_id, Model model) {
        String query = "select * from (SELECT TOP(select count(id) FROM sensor WHERE location_id = :id) value.id v_id, sensor.id s_id, parameter.id p_id, value from value\n" +
                "  INNER JOIN parameter on value.parameter_id = parameter.id\n" +
                "  INNER JOIN sensor on value.sensor_id = sensor.id\n" +
                "  INNER JOIN location on sensor.location_id = location.id\n" +
                "where location_id=:id\n" +
                "               ORDER BY time DESC) t3\n" +
                "order by v_id\n";

        List<ValueEntity> values = em.createNativeQuery(query).setParameter("id", location_id).getResultList();
        return values;
    }

    @RequestMapping(value = "/{id}/getOutsideTemperature", method = RequestMethod.GET)
    public @ResponseBody
    List<ValueEntity> getOutsideTemperature(@PathVariable("id") int location_id, Model model) {
        int outThempSensorId = 8;
        String query = "SELECT TOP(1) value FROM value WHERE sensor_id = " + outThempSensorId;
        List<ValueEntity> values = em.createNativeQuery(query).getResultList();
        return values;
    }

    @RequestMapping(value = "/{id}/getValues", method = RequestMethod.GET)
    public @ResponseBody
    List<ValueEntity> getValues(@PathVariable("id") int location_id, Model model) {
        List<SensorEntity> sensors = sensorRepository.findAllByLocationId(location_id);

        List<ValueEntity> values = new ArrayList<>();
        Integer sensorId;
        for (SensorEntity sensor : sensors){
            sensorId = sensor.getId();
            String query = "SELECT top (1) value, sublocation, parameter_id from value\n" +
                    "  INNER JOIN sensor on value.sensor_id = sensor.id\n" +
                    "  INNER JOIN location on sensor.location_id = location.id\n" +
                    "WHERE sensor_id = :id\n" +
                    "ORDER BY TIME DESC";

            values.addAll(em.createNativeQuery(query).setParameter("id", sensorId).getResultList());
        }
        if(location_id == 6) System.out.println(values.toString());
        return values;
    }

    @RequestMapping(value = "/{id}/getSensorIds", method = RequestMethod.GET)
    public @ResponseBody
    List<SensorEntity> getSensorIds(@PathVariable("id") int location_id, Model model) {
        String query = "select sensor_id from value\n" +
                "INNER JOIN sensor on value.sensor_id = sensor.id\n" +
                "WHERE sensor.location_id = :id\n" +
                "  GROUP BY sensor_id\n" +
                "ORDER BY sensor_id";
        List<SensorEntity> sensors = em.createNativeQuery(query).setParameter("id", location_id).getResultList();
        return sensors;
    }

    @RequestMapping(value = "/{id}/getFailureColors", method = RequestMethod.GET)
    public @ResponseBody
    List<String> getFailures(@PathVariable("id") int location_id, Model model) {
        List<SensorEntity> sensors = sensorRepository.findSensorIdsByLocationId(location_id);
        List<String> colors = new ArrayList<>();
        Integer sensorId;
        String color;
        for (SensorEntity sensor : sensors){
            sensorId = sensor.getId();
            String query =
                    "select 'red' from (select TOP(1) value, parameter_id from value\n" +
                            "  inner join sensor on value.sensor_id = sensor.id\n" +
                            "where sensor_id = :id ORDER BY time desc) t\n" +
                            "  INNER JOIN parameter on t.parameter_id = parameter.id\n" +
                            "  INNER JOIN limit on parameter.limit_id = limit.id\n" +
                            "where ((value < limit.min_value) or (value > limit.max_value))";
            try{
                color = em.createNativeQuery(query).setParameter("id", sensorId).getResultList().get(0).toString();
//                System.out.println(color);
                colors.add("red");
            }catch (Exception e){
                colors.add("white");
            }

        }
        System.out.println(colors);
        return colors;
    }
}