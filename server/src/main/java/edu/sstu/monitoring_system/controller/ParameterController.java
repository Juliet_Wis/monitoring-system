package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.*;
import edu.sstu.monitoring_system.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ParameterController {
    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    SensorRepository sensorRepository;

    @Autowired
    SensorTypeRepository sensorTypeRepository;

    @Autowired
    UnitRepository unitRepository;

    @Autowired
    ValueRepository valueRepository;

    @RequestMapping("/parameter")
    public String process(){
        // save a single ParameterEntity
        //parameterRepository.save(new ParameterEntity("Temperature", 1));

        // save a list of Customers
        //parameterRepository.save(Arrays.asList(new ParameterEntity("Temperature", 1)));

        String result = "";

        for(ParameterEntity parameter : parameterRepository.findAll()){
            result += parameter.toString() + "<br>";
        }
        for(SensorEntity parameter : sensorRepository.findAll()){
            result += parameter.toString() + "<br>";
        }
        for(SensorTypeEntity parameter : sensorTypeRepository.findAll()){
            result += parameter.toString() + "<br>";
        }
        for(UnitEntity parameter : unitRepository.findAll()){
            result += parameter.toString() + "<br>";
        }
        for(ValueEntity parameter : valueRepository.findAll()){
            result += parameter.toString() + "<br>";
        }
        return "parameter";
    }
}
