package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.ValueEntity;
import edu.sstu.monitoring_system.repo.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ValueController {
    @Autowired
    ValueRepository valueRepository;

    @RequestMapping("/value")
    public String handleRequest(Model model) {
        List<ValueEntity> values = new ArrayList<>();
        for (ValueEntity value : valueRepository.findAll()){
            values.add(value);
        }
        model.addAttribute("values", values);
        return "valuePage";
    }
}
