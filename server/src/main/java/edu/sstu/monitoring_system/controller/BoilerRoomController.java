package edu.sstu.monitoring_system.controller;

import edu.sstu.monitoring_system.model.MessageEntity;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.repo.MessageRepository;
import edu.sstu.monitoring_system.repo.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BoilerRoomController {

    @Autowired
    SensorRepository sensorRepository;

    @Autowired
    MessageRepository messageRepository;

    @PersistenceContext
    EntityManager em;

    @RequestMapping(value = { "/", "/home", "/boiler_room"}, method = RequestMethod.GET)
    public String homePage(Model model) {
        return "boilerRoomPage";
    }

    @RequestMapping(value = "/showMessage", method = RequestMethod.GET)
    public @ResponseBody
    List<MessageEntity> getSensorParamValues(Model model) {
        List<MessageEntity> messages= new ArrayList<>();

            String query = "SELECT message.title t1, message.text t2, location.name t3, sensor.name t4, value.time t5 from value\n" +
                    "INNER JOIN  parameter on value.parameter_id = parameter.id\n" +
                    "INNER JOIN limit on parameter.limit_id = limit.id\n" +
                    "  INNER JOIN message on limit.message_id = message.id\n" +
                    "  INNER JOIN sensor on value.sensor_id = sensor.id\n" +
                    "  INNER JOIN location on sensor.location_id = location.id\n" +
                    "where (value < limit.min_value or value > limit.max_value) and time > (dateadd(SECOND, -22, (select top(1) time from value order by time DESC)));";

            messages.addAll(em.createNativeQuery(query).getResultList());

//        for (MessageEntity messageEntity:messages
//             ) {
//            System.out.println(messageEntity.getText());
//        }
        System.out.println("messages  - " + messages.size());
        return messages;
    }
}
