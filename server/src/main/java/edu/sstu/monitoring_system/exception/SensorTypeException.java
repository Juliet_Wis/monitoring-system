package edu.sstu.monitoring_system.exception;

public class SensorTypeException extends Exception {

    public SensorTypeException(String s) {
        super(s);
    }

    public SensorTypeException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
