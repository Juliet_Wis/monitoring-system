package edu.sstu.monitoring_system.service;

import edu.sstu.monitoring_system.model.ParameterEntity;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.model.ValueEntity;
import edu.sstu.monitoring_system.repo.ParameterRepository;
import edu.sstu.monitoring_system.repo.ValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class GeneratorService {

    private static double consumption = 10;
    private final ValueRepository valueRepository;
    private final SensorService sensorService;
    private final ParameterRepository parameterRepository;
    private final Random rnd = new Random(System.currentTimeMillis());;

    @Autowired
    public GeneratorService(
            ValueRepository valueRepository,
            SensorService sensorService, ParameterRepository parameterRepository) {
        this.valueRepository = valueRepository;
        this.sensorService = sensorService;
        this.parameterRepository = parameterRepository;
    }

    // running every 20 seconds
    @Scheduled(fixedRate = 20000)
    public void generateValue() {
        System.out.println("Start update...");
        List<SensorEntity> sensors = sensorService.findAll();
        List<ParameterEntity> parameters = parameterRepository.findAll();
        List<ValueEntity> valueEntities = new ArrayList<>();
        for (SensorEntity sensor : sensors) {
            if (sensor.getId() == 1 ||
                    sensor.getId() == 5 ||
                    sensor.getId() == 9 ||
                    sensor.getId() == 13 ||
                    sensor.getId() == 17){
                double rangeMin = consumption+10, rangeMax = consumption + 20;

                double value = rangeMin + (rangeMax - rangeMin) * rnd.nextDouble();
                value = Math.round(value * 100.0) / 100.0;
                valueEntities.add(new ValueEntity(value, new Date(), sensor, parameters.get(2)));
            } else if (sensor.getId() == 3 ||
                    sensor.getId() == 7 ||
                    sensor.getId() == 11||
                    sensor.getId() == 15){
                double rangeMin = consumption, rangeMax = consumption + 10;

                double value = rangeMin + (rangeMax - rangeMin) * rnd.nextDouble();
                value = Math.round(value * 100.0) / 100.0;
                valueEntities.add(new ValueEntity(value, new Date(), sensor, parameters.get(2)));
            }

            else if (sensor.getId() == 2 ||
                    sensor.getId() == 4 ||
                    sensor.getId() == 6 ||
                    sensor.getId() == 8 ||
                    sensor.getId() == 10 ||
                    sensor.getId() == 12 ||
                    sensor.getId() == 14 ||
                    sensor.getId() == 16){
                double rangeMin = 50, rangeMax = 100;
                double value = rangeMin + (rangeMax - rangeMin) * rnd.nextDouble();
                value = Math.round(value * 100.0) / 100.0;
                valueEntities.add(new ValueEntity(value, new Date(), sensor, parameters.get(0)));
            }else if (sensor.getId() == 18){
                double rangeMin = 0.3, rangeMax = 0.6;
                double value = rangeMin + (rangeMax - rangeMin) * rnd.nextDouble();
                value = Math.round(value * 100.0) / 100.0;
                valueEntities.add(new ValueEntity(value, new Date(), sensor, parameters.get(1)));
            }else if (sensor.getId() == 19){
                double rangeMin = 20, rangeMax = 30;
                double value = rangeMin + (rangeMax - rangeMin) * rnd.nextDouble();
                value = Math.round(value * 100.0) / 100.0;
                valueEntities.add(new ValueEntity(value, new Date(), sensor, parameters.get(3)));
            }
        }
        consumption+=5;
        valueRepository.saveAll(valueEntities);
        System.out.println("Update completed");
    }
}