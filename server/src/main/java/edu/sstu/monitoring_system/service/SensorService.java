package edu.sstu.monitoring_system.service;

import edu.sstu.monitoring_system.exception.SensorTypeException;
import edu.sstu.monitoring_system.model.SensorEntity;

import java.util.List;

public interface SensorService {
    SensorEntity createNew(String name, int sensorTypeId, String description) throws SensorTypeException;

    List<SensorEntity> findByLocationId(int locationId);

    List<SensorEntity> findAll();
}
