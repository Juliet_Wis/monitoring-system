package edu.sstu.monitoring_system.service;

import edu.sstu.monitoring_system.model.AuthorityEntity;
import edu.sstu.monitoring_system.model.UserEntity;
import edu.sstu.monitoring_system.repo.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserCrudRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(s).get(0);
        if(userEntity == null) throw new UsernameNotFoundException("Username does not exist.");
        List<GrantedAuthority> authorities = buildUserAuthority(userEntity.getAuthorities());
        return buildUserForAuthentication(userEntity, authorities);
    }

    public UserEntity findByUsername(String username) throws UsernameNotFoundException{
        UserEntity user = userRepository.findByUsername(username).get(0);
        if(user == null){
            throw new UsernameNotFoundException("Username does not exist.");
        }
        return user;
    }

    private User buildUserForAuthentication(UserEntity user, List<GrantedAuthority> authorities){
        return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
    }

    private  List<GrantedAuthority> buildUserAuthority(Set<AuthorityEntity> authorities){
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        for(AuthorityEntity auth : authorities){
            setAuths.add(new SimpleGrantedAuthority(auth.getAuthority()));
        }

        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
        return result;
    }
}