package edu.sstu.monitoring_system.service;

import edu.sstu.monitoring_system.model.UserEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService {
    UserEntity findByUsername(String username) throws UsernameNotFoundException;
}