package edu.sstu.monitoring_system.service;

import edu.sstu.monitoring_system.exception.SensorTypeException;
import edu.sstu.monitoring_system.model.SensorEntity;
import edu.sstu.monitoring_system.model.SensorTypeEntity;
import edu.sstu.monitoring_system.repo.SensorRepository;
import edu.sstu.monitoring_system.repo.SensorTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SensorServiceImpl implements SensorService {

    private final SensorRepository sensorRepository;
    private final SensorTypeRepository sensorTypeRepository;

    @Autowired
    public SensorServiceImpl(
            SensorRepository sensorCrudRepository,
            SensorTypeRepository sensorTypeRepository) {
        this.sensorRepository = sensorCrudRepository;
        this.sensorTypeRepository = sensorTypeRepository;
    }

    @Override
    public SensorEntity createNew(String name, int sensorTypeId, String description) throws SensorTypeException {
        SensorTypeEntity sensorType = sensorTypeRepository.findById(sensorTypeId).orElse(null);
        if (sensorType == null) {
            throw new SensorTypeException(String.format("Sensor type with id: %d does not exist", sensorTypeId));
        }

        SensorEntity sensor = new SensorEntity(name, sensorType, description);

        return sensorRepository.save(sensor);
    }

    @Override
    public List<SensorEntity> findByLocationId(int locationId) {
        List<SensorEntity> sensors = sensorRepository.findAllByLocationId(locationId);
        return sensors;
    }

    @Override
    public List<SensorEntity> findAll() {
        return sensorRepository.findAll();
    }
}