<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
    <script src="/bootstrap-4.1.0/js/bootstrap.min.js"></script>
    <script src="/webjars/d3js/3.5.10/d3.min.js"></script>
    <style>
        body {
            font: 10px sans-serif;
            margin: 0;
        }

        .axis path {
            fill: none;
            stroke: #bbb;
            shape-rendering: crispEdges;
        }

        .axis text {
            fill: #555;
        }

        .axis line {
            stroke: #e7e7e7;
            shape-rendering: crispEdges;
        }

        .axis .axis-label {
            font-size: 14px;
        }

        .line {
            fill: none;
            stroke-width: 1px;
        }

    </style>
    <link href="/bootstrap-4.1.0/css/bootstrap.min.css"rel="stylesheet" id="bootstrap-css">
    <link href="/style/manometer.css" rel="stylesheet">
    <script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
    <script src="/bootstrap-4.1.0/js/bootstrap.min.js"></script>
    <script src="/javascript/imageMapster/ImageMapster.min.js"></script>
</head>
<body>
<%--<h2>${name}</h2>--%>
<h1>График</h1>
<script>
    var data = [];
    $.getJSON("/getValues", function (d) {
        data = d;
        console.log(data);
    });

    console.log(data);



//    var data = [],
    var    nums = 120, // количество точек
        currentTime = +new Date();

    for (var i = nums - 1; i >= 0; i--) {
        data.push({
            value: Math.random(),
            time: currentTime - i*1000
        });
    }



    var chart = new Chart(data);

    function addDataToChart() {
        currentTime += 1000;

        chart.addData({
            value: Math.random(),
            time: currentTime
        });
    }

    setInterval(function() {
        addDataToChart();
    }, 1000);


    function Chart(data) {
        /* *************************************************************** */
        /* public methods */
        /* *************************************************************** */
        this.addData = addData;


        /**
         * Добавляет данные в конец плавно сдвигая график влево, после окончания анимации удаляет старые данные, оставляе размер данных в исходном состоянии
         * @param newData
         */
        function addData(newData) {
            if (!$.isArray(newData)) {
                newData = [newData];
            }

            svg.selectAll(".area")
                .each(function() {
                    // выстраиваем все анимации в очередь, чтоб все было плавненько и не накладывалось
                    var transition = newSafeTransition(this)
                        .duration(transitionDuration)
                        .ease("linear")
                        .each("start", startTransition)
                        .each("end", endTransition)
                        .attr('transform', function() {
                            // двигаем график влево на необходимое количество точек
                            return 'translate(' + (-this.getBBox().width / data.length * newData.length) + ',0) scale(1,1)';
                        });

                    // Сохраняем в очередь
                    if (!this.__transitionsQueue__) {
                        this.__transitionsQueue__ = [];
                    }

                    this.__transitionsQueue__.push(transition);


                    /**
                     * Начало анимации
                     */
                    function startTransition() {
                        // обновляем график
                        Array.prototype.push.apply(data, newData);

                        var updatedData = [], oldData = [];

                        for (var i = 0; i < data.length; i++) {
                            if (i >= newData.length) {
                                updatedData.push(data[i]);
                            }
                            if (i < data.length - newData.length) {
                                oldData.push(data[i]);
                            }
                        }

                        initAxes(updatedData);

                        // анимируем ось x
                        svg.selectAll(".x.axis")
                            .transition()
                            .duration(transitionDuration)
                            .ease("linear")
                            .call(xAxis);

                        // анимируем ось y
                        svg.selectAll(".y.axis")
                            .transition()
                            .duration(transitionDuration)
                            .ease("linear")
                            .call(yAxis);

                        resetInlineStyles();

                        initAxes(oldData);

                        svg.selectAll(".area")
                            .attr("d", area);
                    }

                    /**
                     * Завершение анимации
                     */
                    function endTransition() {
                        // удаляем из очереди
                        this.__transitionsQueue__.shift();

                        // подчищаем старые данные
                        data.splice(0, newData.length);

                        initAxes();

                        resetInlineStyles();

                        svg.selectAll(".area")
                            .attr("d", area);

                        // возвращаем график на исходную
                        svg.selectAll(".area")
                            .attr('transform', null);
                    }
                });
        }


        /* *************************************************************** */
        /* private variables */
        /* *************************************************************** */
        var width,
            height,
            margin,
            innerWidth,
            innerHeight,
            xLabel = "Ось X",
            yLabel = "Ось Y",
            title = "Плавное обновление графика",
            axisColor = "black",
            transitionDuration = 1000,
            svg,
            gradient,
            area,
            x,
            y,
            xAxis,
            yAxis,
            color = d3.scale.category10(),
            resizeTimer = false,
            tickValues = [];

        data = data || [];




        /* *************************************************************** */
        /* private methods */
        /* *************************************************************** */

        /**
         * Рассчитываем размеры
         */
        function initDimensions() {
            width = window.innerWidth-5;
            height = window.innerHeight-5;

            margin = {
                top: Math.max(height*0.1, 22),
                right: Math.max(height*0.1, 95),
                bottom:  Math.max(height*0.1, 75),
                left: width*0.02
            };

            innerWidth = width - margin.left - margin.right;
            innerHeight = height - margin.top - margin.bottom;
        }


        /**
         * Настройка осей
         * @param {Object[]} [axesData=data]
         */
        function initAxes(axesData) {
            axesData = axesData || data;

            x = d3.time.scale()
                .domain(d3.extent(axesData, function(d) { return d.time; }))
                .range([0, innerWidth]);

            y = d3.scale.linear()
                .domain(d3.extent(axesData, function(d) { return d.value; }))
                .range([innerHeight, 0]);


            xAxis = d3.svg.axis()
                .scale(x)
                .tickValues(getTickValues.bind(this, x, innerWidth, 5))
                .tickFormat(d3.time.format("%H:%M:%S"))
                .tickSize(-innerHeight)
                .tickPadding(10)
                .tickSubdivide(true)
                .orient("bottom");

            yAxis = d3.svg.axis()
                .scale(y)
                .tickValues(getTickValues.bind(this, y, innerHeight, 5))
                .tickFormat(d3.format(",.2f"))
                .tickPadding(10)
                .tickSize(-innerWidth+1)
                .tickSubdivide(true)
                .orient("right");
        }


        /**
         * Расчет значений отсечек
         * @param scale
         * @param size - ширина или высота оси
         * @param count - количество отсечек
         * @returns {Object[]}
         */
        function getTickValues(scale, size, count) {
            var id = [size, count].join('_');

            // расчет начальных значений
            if (!tickValues[id]) {
                count = Math.max(count, 2);

                tickValues[id] = {
                    distance: size/count,
                    values: []
                };

                for (var i = 0; i < count; i++) {
                    tickValues[id].values.push(scale.invert(tickValues[id].distance/2 + i*tickValues[id].distance))
                }
            } else {
                var lastIndex = tickValues[id].values.length - 1;

                var firstDistance = scale(tickValues[id].values[0]),
                    lastDistance = size - scale(tickValues[id].values[lastIndex]);

                // удаляем левый, если уходит за груницу
                if (firstDistance < 0) {
                    tickValues[id].values.shift();
                }

                // удаляем правый, если уходит за границу
                if (lastDistance < 0) {
                    tickValues[id].values.pop();
                }

                // добавляем новую вторую слева если она слишком отдалилась от самой левой
                if (firstDistance > tickValues[id].distance) {
                    tickValues[id].values.unshift(scale.invert(firstDistance - tickValues[id].distance));
                }

                // добавляем новую вторую справа если она слишком отдалилась от самой правой
                if (lastDistance > tickValues[id].distance) {
                    tickValues[id].values.push(scale.invert(size - (lastDistance - tickValues[id].distance)));
                }
            }

            return tickValues[id].values;
        }


        /**
         * Создаем новый transition и добавляем его в очередь, если анимация уже выполняется
         * @param node
         * @return {*}
         */
        function newSafeTransition(node) {
            var transitionsQueue = node.__transitionsQueue__;
            if ( transitionsQueue && transitionsQueue.length ) {
                return transitionsQueue[ transitionsQueue.length - 1 ].filter( function(){ return this === node; } ).transition();
            } else {
                return d3.select(node).transition();
            }
        }


        /**
         * Задаем инлайн стили
         */
        function resetInlineStyles() {
            var xGs = svg.selectAll('.x.axis')
                .selectAll("g.tick");

            xGs.selectAll('text')
                .attr("y", 7)
                .attr("x", -3)
                .attr("dy", ".35em")
                .attr("transform", "rotate(-30)")
                .style("text-anchor", "end");
        }


        /**
         * событие изменения размера окна
         */
        function windowResize() {
            initDimensions();

            d3.select("body svg")
                .attr("width", innerWidth + margin.left + margin.right)
                .attr("height", innerHeight + margin.top + margin.bottom);

            svg.select("g.container")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            initAxes();

            svg.selectAll(".x.axis")
                .attr("transform", "translate(0," + innerHeight + ")")
                .call(xAxis)
                .selectAll(".axis-label")
                .attr("x", innerWidth);

            svg.selectAll(".y.axis")
                .attr("transform", "translate(" + innerWidth + ",0)")
                .call(yAxis);

            resetInlineStyles();

            svg.select("#clip rect")
                .attr("width", innerWidth)
                .attr("height", innerHeight);

            svg.selectAll(".area")
                .attr("d", area);

            area.y0(innerHeight);

            svg.selectAll(".title text")
                .attr("x", innerWidth/2);
        }


        /**
         * Инициализация
         */
        function init() {
            initDimensions();
            initAxes();

            // добавляем svg
            svg = d3.select("body").append("svg")
                .attr("width", innerWidth + margin.left + margin.right)
                .attr("height", innerHeight + margin.top + margin.bottom)
                .append("g")
                .attr("class", "container")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            // градиент для графика
            gradient = svg.append("defs")
                .append("linearGradient")
                .attr("id", "areaGradient")
                .attr("x1", "0")
                .attr("y1", "0")
                .attr("x2", "0")
                .attr("y2", "100%")
                .attr("spreadMethod", "pad");

            gradient.append("stop")
                .attr("offset", "0%")
                .attr("stop-color", color())
                .attr("stop-opacity", 0.7);

            gradient.append("stop")
                .attr("offset", "100%")
                .attr("stop-color", color())
                .attr("stop-opacity", 0.3);


            // ось x
            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + innerHeight + ")")
                .call(xAxis)
                .append("text")
                .attr("class", "axis-label")
                .attr("dy", "4em")
                .attr("x", innerWidth)
                .style({
                    "text-anchor": "end",
                    "font-size": "11px"
                })
                .text(xLabel);

            resetInlineStyles();

            // ось y
            svg.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + innerWidth + ",0)")
                .call(yAxis)
                .append("text")
                .attr("class", "axis-label")
                .attr("transform", "rotate(90)")
                .attr("x", 30)
                .attr("dy", "-5em")
                .style({
                    "text-anchor": "end",
                    "font-size": "11px"
                })
                .text(yLabel);

            svg.selectAll('.axis .domain')
                .style("stroke", axisColor);

            // ограничиваем зону видимости
            svg.append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", innerWidth)
                .attr("height", innerHeight);


            // заголовок
            svg.append("g")
                .attr("class", "title")
                .append("text")
                .attr("dy", "-.71em")
                .attr("x", innerWidth/2)
                .style({
                    "text-anchor": "middle",
                    "font-size": "14px"
                })
                .text(title);


            // график
            area = d3.svg.area()
                .interpolate("linear")
                .x(function(d) {
                    return x(d.time);
                })
                .y0(innerHeight)
                .y1(function(d) {
                    return y(d.value);
                });

            svg.append("g")
                .attr("class", "area-g")
                .attr("clip-path", "url(#clip)")
                .selectAll('.area')
                .data([data])
                .enter()
                .append("path")
                .attr("class", "area")
                .attr("stroke", color())
                .attr("stroke-width", "2px")
                .attr("fill", "url(#areaGradient)")
                .style('vector-effect', 'non-scaling-stroke')
                .attr("d", area);


            // вешаем событие изменения размера окна с задержкой
            $(window).resize(function(){
                if (resizeTimer !== false)
                    clearTimeout(resizeTimer);

                resizeTimer = setTimeout(windowResize, 200);
            });
        }

        init();
    }
</script>
<script src="/javascript/sensorDataUpdater.js"></script>
</body>
</html>



