<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Аутентификация</title>
    <link href="/bootstrap-3.0.0/css/bootstrap.min.css"rel="stylesheet" id="bootstrap-css">
    <script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
    <script src="/bootstrap-3.0.0/js/bootstrap.min.js"></script>

    <style>
        .panel > .panel-heading {
            background-image: none;
            background-color: #333;
            color: white;
        }
    </style>
</head>
<body>
        <div class="container">
            <br>
            <br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Вход в систему мониторинга</h4></div>
                    <div class="panel-body" style="margin-top: 15px;">
                        <div class="col-sm-6 col-sm-offset-3">
                            <c:url var="loginUrl" value="/login"/>
                            <form action="${loginUrl}" method="post" class="form-horizontal">
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="username">Логин:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="username" name="username"
                                               placeholder="Введите логин" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="password">Пароль:</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="password" name="password"
                                               placeholder="Введите пароль" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" value="send" class="btn btn-success">Войти</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <c:if test="${param.error != null}">
                    <div class="alert alert-danger">
                        <p>Неправильный логин или пароль.</p>
                    </div>
                </c:if>
                <c:if test="${param.logout != null}">
                    <div class="alert alert-success">
                        <p>Вы успешно вышли из системы.</p>
                    </div>
                </c:if>
            </div>
        </div>
</body>
</html>