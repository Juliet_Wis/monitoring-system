<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><tiles:getAsString name="title" /></title>
    <link href="/bootstrap-4.1.0/css/bootstrap.min.css"rel="stylesheet" id="bootstrap-css">
    <link href="/style/manometer.css" rel="stylesheet">
    <link href="/style/callout.css" rel="stylesheet">
    <link href="/style/main.css" rel="stylesheet">
    <link rel="stylesheet" href="/style/fontawesome/fontawesome-all.css">
</head>
<body>

<div class="container-fluid">
    <div class="bs-callout bs-callout-danger">
        <h4>Перепад температуры выше нормы</h4>
        <p>Необходимо устранить неисправность</p>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="row ">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 text-center center-block">
                            <h1>Горячая вода</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 wrap">
                            <p class="value">G<sub>1</sub> = 5 м<sup>3</sup></p>
                        </div>
                        <div class="col-6 text-center center-block">
                            <div id="therm_1"></div>
                            <p style="color: ${therm2_color}">T<sub>1</sub> = 55<sup>o</sup>C</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center center-block">
                            <img class="img-fluid" id="schema" src="/images/svg/enter_boiler_room.svg"
                                 usemap="#schema">
                            <map id="schema_image_map" name="schema">
                                <area href="#" element="thermometer" shape="circle" title="Перепад температуры выше нормы" coords="1576, 379, 133" />
                                <area href="#" element="flowmeter"  title="Расход в норме" shape="rect" coords="259, 0, 767, 215" />
                            </map>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-6 wrap">
                            <p class="value">G<sub>3</sub> = 5 м<sup>3</sup></p>
                        </div>
                        <div class="col-6 text-center center-block">
                            <div id="therm_3"></div>
                            <p style="color: ${therm2_color}">T<sub>3</sub> = 53<sup>o</sup>C</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center center-block">
                            <img class="img-fluid" src="/images/svg/out_boiler_room.svg" id="schema_2"
                                 usemap="#schema_2" >
                            <map id="schema_2_image_map" name="schema_2">
                                <area href="#" element="thermometer" shape="circle" title="Перепад температуры выше нормы" coords="1576, 379, 133" />
                                <area href="#" element="flowmeter" title="Расход в норме" shape="rect" coords="259, 0, 767, 215" />
                            </map>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-6 text-center center-block">
                            <p style="color: ${press_delta_color}">&#916G<sub>1</sub>P<sub>2</sub> = 1</p>
                        </div>
                        <div class="col-6 text-center center-block">
                            <p style="color: ${press_delta_color}">&#916T<sub>1</sub>P<sub>2</sub> = 1</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="row">
                <div class="col-12">
                    <h1>t<sub>нв</sub> = 18<sup>o</sup>C</h1>
                    <hr size="5">
                    <p>Построить график:</p>

                    <form>
                        <input type="checkbox">G<sub>2</sub>
                        <br>
                        <input type="checkbox">G<sub>3</sub>
                    </form>
                    <button>ОК</button>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
<script src="/webjars/d3js/3.5.10/d3.min.js"></script>

<script src="/bootstrap-4.1.0/js/bootstrap.min.js"></script>

<script src="/javascript/imageMapster/imageMapster.js"></script>
<script src="/javascript/thermometer.js"></script>
<script src="/javascript/manometer.js"></script>

<script>
    var imgRoot ='/images/svg/enter_boiler_room.svg';
    var maps = [$('#schema'), $('#schema_2')] ;
//    maps.forEach(function (map) {
//        map.mapster({
//            mapKey: 'element',
//            areas: [{
//                key: 'thermometer',
//                staticState: true,
//                render_select: {
//                    fillOpacity: 0.5,
//                    fillColor: 'ff0000'
//                }},
//                {
//                    key: 'flowmeter',
//                    staticState: true,
//                    render_select: {
//                        fillOpacity: 0.5,
//                        fillColor: '00ff00'
//                    }
//                }
//            ]
//        })
//            .mapster('snapshot')
//            .mapster('rebind', {
//                mapKey: 'element'
//            },true);
//    });

    function indicateSchemaWarning(map){

        map.mapster({
            mapKey: 'element',
            areas: [{
                key: 'thermometer',
                staticState: true,
                render_select: {
                    fillOpacity: 0.5,
                    fillColor: 'ff0000'
                }},
                {
                    key: 'flowmeter',
                    staticState: true,
                    render_select: {
                        fillOpacity: 0.5,
                        fillColor: 'ff0000'
                    }
                }
            ]
        })
            .mapster('snapshot')
            .mapster('rebind', {
                mapKey: 'element'
            },true);
    }

    function indicateSchemaOk(map){
        map.mapster({
            mapKey: 'element',
            areas: [{
                key: 'thermometer',
                staticState: true,
                render_select: {
                    fillOpacity: 0,
                    fillColor: 'ff0000'
                }},
                {
                    key: 'flowmeter',
                    staticState: true,
                    render_select: {
                        fillOpacity: 0,
                        fillColor: 'ff0000'
                    }
                }
            ]
        })
            .mapster('snapshot')
            .mapster('rebind', {
                mapKey: 'element'
            },true);

    }


    function stopIndication() {
        clearInterval(timerId);
    }

    function indicateWarning() {
        var timerId = setInterval(function() {
            indicateSchemaWarning(maps[0]);
            var tmt = setTimeout(  function () {
                indicateSchemaOk(maps[0]);
            }, 1000);
        }, 2000);
    }


    $(document).ready(function() {
        getThermometer(55, "#therm_1");
        getThermometer(53, "#therm_3");

        var thempDelta = 2;

        if (thempDelta > 0.5) indicateWarning();
        else stopIndication();


    });
</script>

</body>
</html>
