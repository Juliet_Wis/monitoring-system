<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page contentType="text/html;charset=utf-8" %>
<!DOCTYPE html>
<html lang="en">
<style>
    h1{
        margin:0;
    }
</style>
<head>
    <meta charset="UTF-8">
    <title><tiles:getAsString name="title" /></title>
    <link href="/bootstrap-4.1.0/css/bootstrap.min.css"rel="stylesheet" id="bootstrap-css">
    <link href="/style/menu.css" rel="stylesheet">
    <link href="/style/manometer.css" rel="stylesheet">
    <link href="/style/callout.css" rel="stylesheet">
    <link href="/style/main.css" rel="stylesheet">
    <link href="/style/fontawesome/fontawesome-all.css" rel="stylesheet">
    <script src="/webjars/jquery/3.3.1-1/jquery.min.js"></script>
    <script src="/bootstrap-4.1.0/js/bootstrap.min.js"></script>
    <script src="/webjars/d3js/3.5.10/d3.min.js"></script>
</head>

<body>
<tiles:insertAttribute name="menu" />
<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="menuend" />
<tiles:insertAttribute name="footer" />
<script>
    $(document).ready(function () {
        $('label.tree-toggler').click(function () {
            $(this).parent().children('ul.tree').toggle(300);
        });

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    });
</script>
</body>
</html>