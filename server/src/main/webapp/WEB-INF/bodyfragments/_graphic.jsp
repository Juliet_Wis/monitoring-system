<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<a href="" id="back">Назад</a>
<form id="datetimeForm">
    <input id="fromTime" type="datetime-local" required = "required"/>
    <input id="toTime" type="datetime-local" required = "required"/>
    <br>
    <button type="submit">Получить значения</button>
</form>
<div id="graphics">
    <c:forEach var="sensor" items="${sensors}">
        <div class="paramcont" style="width: 100%; height: 400px; margin-bottom: 80px" id="${sensor.id}" name="${sensor.name}">
            <canvas style="margin-bottom: 80px;" id="chart-${sensor.id}"></canvas>
        </div>
    </c:forEach>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/javascript/getGraphicData.js"></script>
