<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container-fluid">
    <div id="messages"></div>
    <div class="row">
        <div class="col-8">
            <div class="row ">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 text-center center-block">
                            <h1 id="locationId" style="display: none">${locationId}</h1>
                            <h1>${locationName}</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-10">
                                    <div class="row" id="row-in"></div>
                                    <div class="row" id="row-schema-1"></div>
                                </div>
                                <div class="col-2">
                                    <div class="row" id="row-in-therm"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-10">
                                    <div class="row" id="row-out"></div>
                                    <div class="row" id="row-schema-2"></div>
                                </div>
                                <div class="col-2">
                                    <div class="row" id="row-out-therm"></div>
                                </div>
                            </div>
                            <div class="row" id="row-pump"></div>
                            <div class="row" id="row-schema-pump"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
        <div class="col-3">
            <%--<div class="row">--%>
            <%--<div class="col-12" id="right-block"></div>--%>
            <%--</div>--%>
            <div class="row">
                <div class="col-12" id="right-block"></div>
            </div>
            <div class="row" id="row-delta"></div>
        </div>
    </div>
</div>

<script src="/javascript/rowInserter.js"></script>
<script src="/javascript/initLocationPage.js"></script>
<script src="/javascript/imageMapster/imageMapster.js"></script>
<script src="/javascript/thermometer.js"></script>
<script src="/javascript/manometer.js"></script>