<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=utf-8" %>

<div class="сontainer-fluid">
    <div class="row  text-center center-block">
        <div class="col-md-12  text-center center-block">
            <table class="table  text-center center-block" >
                <h3 class="text-center">Показания датчиков</h3>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Время</th>
                    <th>Датчик</th>
                    <%--<th>Модель</th>--%>
                    <%--<th>Измерительный узел</th>--%>
                    <th>Параметр</th>
                    <th>Значение</th>
                    <th>Единица измерения</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach  items="${values}" var ="value">
                    <tr>
                        <td>${value.id}</td>
                        <td>${value.time}</td>
                        <td>${value.sensor.name}</td>
                        <%--<td>${value.sensor.se}</td>--%>
                        <%--<td>${value.sensor.name}</td>--%>
                        <td>${value.parameter.name}</td>
                        <td>${value.value}</td>
                        <td>${value.parameter.unit.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
