<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="container">
    <br>
    <br>
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Добавление датчика</h4></div>
            <div class="panel-body" style="margin-top: 15px;">
                <div class="col-sm-6 col-sm-offset-3">
                    <%--<c:url var="loginUrl" value="/login"/>--%>
                    <%--<form action="${loginUrl}" method="post" class="form-horizontal">--%>
                    <form action="/saveSensor" method="post" modelAttribute="sensorType"  class="form-horizontal">
                        <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">--%>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Название:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="username" name="username"
                                       placeholder="Введите имя датчика" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="username">Модель:</label>
                            <div class="col-sm-9">
                                <form:select  path="Name" class="form-control">
                                    <form:option value="NONE">Выберите тип датчика</form:option>
                                    <form:options items="${sensorTypes}"  itemValue="Id" itemLabel="Name"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="username">Измерительный узел:</label>
                            <div class="col-sm-9">
                                <form:select  path="name" class="form-control">
                                    <form:option value="NONE">Выберите измерительный узел</form:option>
                                    <form:options items="${locations}" itemValue="Id" itemLabel="Name"></form:options>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="password">Пароль:</label>
                            <div class="col-sm-9">
                                <form:textarea path="address" rows="5" cols="30" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="password">Пароль:</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Введите пароль" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" value="send" class="btn btn-success">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <%--<c:if test="${param.error != null}">--%>
        <%--<div class="alert alert-danger">--%>
        <%--<p>Неправильный логин или пароль.</p>--%>
        <%--</div>--%>
        <%--</c:if>--%>
        <%--<c:if test="${param.logout != null}">--%>
        <%--<div class="alert alert-success">--%>
        <%--<p>Датчик успешно добавлен.</p>--%>
        <%--</div>--%>
        <%--</c:if>--%>
    </div>
</div>