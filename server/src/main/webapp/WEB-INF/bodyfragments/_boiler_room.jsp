<%@ page contentType="text/html;charset=utf-8" %>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-1  text-center center-block"></div>
        <div class="col-md-9  text-center center-block">
            <h1>Схема котельной СГТУ</h1>
            <img id="schema" class="img-fluid text-center center-block" src="/images/boilerRoomFinal.png" alt="" usemap="#schema"  />
            <map name="schema" id="schema_image_map">
                <area href="/location/6" title="Давление" shape="circle" coords="4520, 1755, 96" />
                <area href="/location/2" title="Отопление #2" shape="circle" coords="5164, 733, 93" />
                <area href="/location/2" title="Отопление #2" shape="circle" coords="5794, 733, 93" />
                <area href="/location/4" title="Горячая вода #2" shape="circle" coords="6438, 745, 100" />
                <area href="/location/4" title="Горячая вода #2" shape="circle" coords="7065, 741, 98" />
                <area href="/location/3" title="Горячая вода #1" shape="circle" coords="7074, 4227, 97" />
                <area href="/location/3" title="Горячая вода #1" shape="circle" coords="6441, 4228, 96" />
                <area href="/location/1" title="Отопление #1" shape="circle" coords="5803, 4219, 98" />
                <area href="/location/1" title="Отопление #1" shape="circle" coords="5164, 4218, 98" />
                <area href="/location/1" title="Отопление #1" shape="rect" coords="5277, 4377, 5383, 4627" />
                <area href="/location/1" title="Отопление #1" shape="rect" coords="5923, 4377, 6033, 4623" />
                <area href="/location/3" title="Горячая вода #1" shape="rect" coords="6558, 4374, 6658, 4627" />
                <area href="/location/3" title="Горячая вода #1" shape="rect" coords="7200, 4375, 7302, 4629" />
                <area href="/location/4" title="Горячая вода #2" shape="rect" coords="7201, 306, 7303, 556" />
                <area href="/location/4" title="Горячая вода #2" shape="rect" coords="6562, 303, 6664, 556" />
                <area href="/location/2" title="Отопление #2" shape="rect" coords="5916, 297, 6027, 561" />
                <area href="/location/2" title="Отопление #2" shape="rect" coords="5283, 306, 5388, 559" />
                <area href="/location/5" title="Холодная вода" shape="rect" coords="178, 966, 436, 1080" />
            </map>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-9">
            <div id="messages"></div>
        </div>
    </div>
</div>

<script src="/javascript/imageMapster/imageMapster.js"></script>
<%--<script src="/javascript/initBoilerRoomPage.js"></script>--%>
<script src="/javascript/initLocationPage.js"></script>
<script>
    $('#schema').mapster({
        onClick: function() {
            window.location=this.href;
            return false;
        }
    });
</script>
<script>
    function getMessages() {
        $.getJSON("/showMessage", function (messages) {
            var messageDiv = document.getElementById('messages');
            while (messageDiv.firstChild) {
                messageDiv.removeChild(messageDiv.firstChild);
            }

            messages.forEach(function (msg) {
                var div = document.createElement('div');
                div.className = "bs-callout bs-callout-danger";

                var title = document.createElement('h4');
                title.innerHTML = msg[0];
                div.appendChild(title);

                var text = document.createElement('p');
                text.innerHTML = msg[1];
                div.appendChild(text);

                var location = document.createElement('p');
                location.innerHTML = "Измерительный узел: " + msg[2];
                div.appendChild(location);

                var sensor = document.createElement('p');
                sensor.innerHTML = "Датчик: " + msg[3];
                div.appendChild(sensor);
                var time = document.createElement('p');
                time.innerHTML = "Время: " + new Date(msg[4]).toLocaleString();
                div.appendChild(time);
                messageDiv.appendChild(div);
            });
        });
    };

    window.setInterval(getMessages, 20000);
</script>