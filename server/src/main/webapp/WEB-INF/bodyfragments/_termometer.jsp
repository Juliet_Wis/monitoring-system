<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="thermo" style="font-family: Arial; padding: 10px 20px;"></div>
<script src="/webjars/d3js/3.5.10/d3.min.js"></script>
<script src="/javascript/thermometer.js"></script>
<script>
    $(document).ready(function() {
        getThermometer(30, "#thermo")
    });
</script>