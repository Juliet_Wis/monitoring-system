<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="/style/manometer.css" rel="stylesheet">
<%--MANOMETER--%>
<script>
    $(document).ready(function() {
// вызов функции после потери полем 'userName' фокуса
        $('#userName').blur(function() {
            $.ajax({
                url : 'userServlet',     // URL - сервлет
                data : {                 // передаваемые сервлету данные
                    userName : $('#userName').val()
                },
                success : function(response) {
// обработка ответа от сервера
                    $('#ajaxUserServletResponse').text(response);
                }
            });
        });
    });
</script>
<p />
Ваше имя : <input type="text" id="userName" /><br />
<span style="font-style:italic; font-size:75%">
                сервлет ответит после потери полем курсора</span>
<p />
<strong>Ответ сервлета </strong>:
<span id="ajaxUserServletResponse"></span>


<div class="container">
    <div id="man1"></div>
    <div style="width: 150px">
        <p style="font-weight: bold; color: #1d840e; text-align: center">
            P<sub>1</sub> = ${man2_value} кгс/м<sup>2</sup>
        </p>
    </div>
</div>


<div id="man2"></div>

<script src="/webjars/d3js/3.5.10/d3.min.js"></script>
<script src="/javascript/manometer.js"></script>
<script>
    $(document).ready(function() {
//      SET MANOMETER VALUE AND ID
        getManometer(0.5, "#man1");
        getManometer(0.3, "#man2");
    });
</script>