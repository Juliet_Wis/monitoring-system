<%@ page contentType="text/html;charset=utf-8" %>
<nav class="navbar-primary">
    <a href="#" class="btn-expand-collapse">
        <span class="glyphicon glyphicon-menu-left"><i class="fas fa-align-justify"></i></span></a>
    <ul class="navbar-primary-menu">
        <li>
            <a href="#"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Мнемосхемы</span></a>
        </li>
        <li>
            <ul>
                <li>
                    <a href="/boiler_room"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Котельная</span></a>
                    <a href="/location/1"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Отопление (Потребитель #1)</span></a>
                    <a href="/location/2"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Отопление (Потребитель #2)</span></a>
                    <a href="/location/3"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Горячая вода (Потребитель #1)</span></a>
                    <a href="/location/4"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Горячая вода (Потребитель #2)</span></a>
                    <a href="/location/5"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Холодная вода</span></a>
                    <a href="/location/6"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Давление на выводе горячей воды</span></a>
                </li>
            </ul>
        </li>
        <li>
            <a href="/value"><span class="glyphicon glyphicon-cog"></span><span class="nav-label">Показания</span></a>
            <a href="/logout"><span class="glyphicon glyphicon-cog"></span><span class="nav-label">Выйти</span></a>
        </li>
    </ul>
</nav>
<div class="main-content">