var paramGraphViews = [];

let fromEl = document.getElementById('fromTime');
let toEl = document.getElementById('toTime');
let errorEl = document.getElementById('error');
let graphsEl = document.getElementById('graphics');

initPage();
initGraphViews();

//Инициализация страницы (ссылка назад,динамические ограничения полей ввода даты-времени)
function initPage() {

    let oldURL = document.referrer;
    document.getElementById('back').setAttribute('href', oldURL);


    let nowDate = new Date();
    toEl.setAttribute('max', dateToViewFormat(nowDate));

    nowDate.setMinutes(nowDate.getMinutes() - 1);
    fromEl.setAttribute('max', dateToViewFormat(nowDate));

//Обновление ограничений при изменении даты начала показа значений
    fromEl.onblur = function(){
        let nowDate = new Date();
        toEl.setAttribute('max', dateToViewFormat(nowDate));
        nowDate.setMinutes(nowDate.getMinutes() - 1);
        fromEl.setAttribute('max', dateToViewFormat(nowDate));

        let toDate = new Date(Date.parse(fromEl.value));
        toDate.setMinutes(toDate.getMinutes() + 1);
        toEl.setAttribute('min', dateToViewFormat(toDate));
    };
    //Обновление ограничений при изменении даты окончания показа значений
    toEl.onBlur = function(){
        let nowDate = new Date();
        toEl.setAttribute('max', dateToViewFormat(nowDate));
        nowDate.setMinutes(nowDate.getMinutes() - 1);
        fromEl.setAttribute('max', dateToViewFormat(nowDate));
    }
}

//Инициализация массива paramGraphViews для отображения графиков.
function initGraphViews() {
    let graphs = graphsEl.children;
    for (var i = 0; i < graphsEl.children.length; i++) {
        let id = graphs[i].id;

        graphs[i].hidden = true;

        paramGraphViews[id] = {
            label: graphs[i].name,
            chart: initChart(id, graphs[i].getAttribute("name")),
            DOMElement: graphs[i],
            addData : function (label, data) {
                this.chart.data.labels.push(label);
                this.chart.data.datasets.forEach((dataset) => {
                    dataset.data.push(data);
            });
                this.chart.update();
            },
            clear: function(){
                this.chart.data.labels = [];
                this.chart.data.datasets.forEach((dataset) => {
                    dataset.data = [];
            });
                this.chart.update();
                this.labels = [];
                this.data = [];
            }
        };
    }
}

//AJAX запрос новых значений в диапазоне
$('#datetimeForm').on('submit', function(e) {
    e.preventDefault();
    console.log("Request to : " + window.location.href + "/getData");
    $.post(window.location.href + "/getData",{
            paramIds: Object.keys(paramGraphViews),//.ids,
            from: Date.parse(fromEl.value),
            to: Date.parse(toEl.value)
        },
        function(data) {
            if(data.error != null){
                alert(data.error);
                return;
            }
            else{
                console.log(data);
                paramGraphViews.forEach(function (view) {
                    view.clear();
                    view.DOMElement.hidden = false;
                });
                data.values.forEach(function (value) {
                    let label = labelForTimeWithPercision(value.time, data.percision);
                    paramGraphViews[value.sensorId].addData(label, value.value);
                });
            }
        });

});


function labelForTimeWithPercision(time, percision) {
    var date = new Date(Date.parse(time));
    console.log("PER: " + percision);
    switch(percision){
        case "VALUE":
        {
            return date.toLocaleTimeString();
        }
        case "MINUTE":
        {
            return ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
        }
        case "HOUR":
        {
            return weekday[date.getUTCDay()] + " " + ("0" + date.getHours()).slice(-2) + ":00";
        }
    }

    return undefined;
}

function initChart(id, chartLabel){
    var ctx = document.getElementById("chart-"+id).getContext('2d');
    ctx.height = 400;
    //if(paramGraphViews[id] !== undefined)charts[id].destroy();
    let chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: chartLabel,
                data: [],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ]
            }],
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }

    });
    //  chart.data.datasets[0] = data;
    return chart;
}

function dateToViewFormat(d) {
    return (d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) +
        "T" + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2));
}

var weekday = new Array(7);
weekday[0] =  "Вс";
weekday[1] = "Пн";
weekday[2] = "Вт";
weekday[3] = "Ср";
weekday[4] = "Чт";
weekday[5] = "Пт";
weekday[6] = "Сб";
