function displayFailure(map) {
    map.mapster({
        mapKey: 'color',
        areas: [{
            key: 'red',
            staticState: true,
            render_select: {
                fillOpacity: 0.5,
                fillColor: 'ff0000'
            }},
            {
                key: 'white',
                staticState: true,
                render_select: {
                    fillOpacity: 0,
                    fillColor: 'ff0000'
                }
            }
        ]
    })
        .mapster('snapshot')
        .mapster('rebind', {
            mapKey: 'color'
        },true);
}

function insertRow(rowDiv, thermDiv, sublocation, g, t){
    while (rowDiv.firstChild) {
        rowDiv.removeChild(rowDiv.firstChild);
    }
    while (thermDiv.firstChild) {
        thermDiv.removeChild(thermDiv.firstChild);
    }
    rowDiv.insertAdjacentHTML('afterBegin',
        '<div class="col-6 wrap">\n' +
        '<p class="value">G<sub>'+sublocation+'</sub> = '+ g +' м<sup>3</sup></p>\n' +
        '</div>\n' +
        '<div class="col-6 text-center center-block">\n' +
        '<p>T<sub>'+sublocation+'</sub> = '+ t+ '<sup>o</sup>C</p>\n' +
        '</div>');
    thermDiv.insertAdjacentHTML('afterBegin', '<div id="therm_'+ sublocation +'"></div>');
    getThermometer(t, "#therm_"+sublocation);
}

function insertColdRow(rowDiv, sublocation, g){
    while (rowDiv.firstChild) {
        rowDiv.removeChild(rowDiv.firstChild);
    }
    rowDiv.insertAdjacentHTML('afterBegin', '<div class="col-6 wrap">\n' +
        '<p class="value">G<sub>'+sublocation+'</sub> = '+ g +' м<sup>3</sup></p>\n' +
        '</div>\n' +
        '<div class="col-6 text-center center-block"></div>');
}

function insertPumpRow(rowDiv, sublocation, p){
    while (rowDiv.firstChild) {
        rowDiv.removeChild(rowDiv.firstChild);
    }
    rowDiv.insertAdjacentHTML('afterBegin',
        '<div class="col-6 wrap">\n' +
        '</div>\n' +
        '<div class="col-6 text-center center-block">\n' +
        '<div id="man_'+ sublocation +'"></div>\n' +
        '<p>P<sub>'+sublocation+'</sub> = '+ p+ 'МПа</p>\n' +
        '</div>');
    getManometer(p, "#man_"+sublocation);
}

function insertDeltaRow(rowDiv, sublocations, parameters, values) {
    while (rowDiv.firstChild) {
        rowDiv.removeChild(rowDiv.firstChild);
    }
    rowDiv.insertAdjacentHTML('beforeEnd', '\n' +
        '<div class="col-12">' +
        '<div class="row"><p>&#916'+parameters[0]+'<sub>'+sublocations[0]+'</sub>'+parameters[0]+'<sub>'+sublocations[1]+'</sub> = '+values[0]+'</p></div>\n' +
        '<div class="row"><p>&#916'+parameters[1]+'<sub>'+sublocations[0]+'</sub>'+parameters[1]+'<sub>'+sublocations[1]+'</sub> = '+values[1]+'</p></div>\n' +
        '</div>');
}

function insertSchemaRow(schemaCount, colors){
    var j = 0;
    var title_1 =" ";
    var title_2 = " ";
    var rowSchema = document.getElementById("row-schema-1");
    while (rowSchema.firstChild) {
        rowSchema.removeChild(rowSchema.firstChild);
    }
    var i = 1;
    rowSchema.insertAdjacentHTML('beforeEnd', '<div class="col-12 text-center center-block">\n' +
        '<img class="img-fluid" id="schema_'+ i +'" src="/images/svg/enter_boiler_room.svg"\n' +
        '     usemap="#schema_'+ i +'">\n' +
        '<map id="schema_image_map_'+ i +'" name="schema_'+ i +'">\n' +
        '<area href="#" color="'+colors[j++]+'" element="flowmeter"  title="'+title_2+'" shape="rect" coords="259, 0, 767, 215" />\n' +
        '<area href="#" color="'+colors[j++]+'" element="thermometer" shape="circle" title="'+title_1+'" coords="1576, 379, 133" />\n' +
        '</map></div>');
    rowSchema = document.getElementById("row-schema-2");
    while (rowSchema.firstChild) {
        rowSchema.removeChild(rowSchema.firstChild);
    }
    i++;
    rowSchema.insertAdjacentHTML('beforeEnd', '<div class="col-12 text-center center-block">\n' +
        '<img class="img-fluid" id="schema_'+ i +'" src="/images/svg/out_boiler_room.svg"\n' +
        '     usemap="#schema_'+ i +'">\n' +
        '<map id="schema_image_map_'+ i +'" name="schema_'+ i +'">\n' +
        '<area href="#" color="'+colors[j++]+'" element="flowmeter"  title="'+title_2+'" shape="rect" coords="259, 0, 767, 215" />\n' +
        '<area href="#" color="'+colors[j++]+'" element="thermometer" shape="circle" title="'+title_1+'" coords="1576, 379, 133" />\n' +
        '</map></div>');
    for (i = 1; i <= schemaCount; i++){
        displayFailure($('#schema_'+i));
    }
}

function insertColdSchemaRow(){
    var rowSchema = document.getElementById("row-schema-1");
    while (rowSchema.firstChild) {
        rowSchema.removeChild(rowSchema.firstChild);
    }
    rowSchema.insertAdjacentHTML('beforeEnd',
        '<div class="col-12 text-center center-block">' +
        '<img class="img-fluid" id="schema_cold" src="/images/svg/cold_water.svg">' +
        '</div>');
}


function insertPumpSchemaRow(){
    var rowSchema = document.getElementById("row-schema-pump");
    rowSchema.insertAdjacentHTML('beforeEnd',
        '<div class="col-12 text-center center-block">' +
        '<img class="img-fluid" id="schema_pump" src="/images/svg/pump.svg">' +
        '</div>');
}

function insertThemperature(div, t){
    if (div.firstChild) div.removeChild(div.firstChild);
    div.insertAdjacentHTML('afterBegin', '<h1>t<sub>нв</sub> = '+ t +'<sup>o</sup>C</h1>');
}

function stopIndication(timer) {
    clearInterval(timer);
}