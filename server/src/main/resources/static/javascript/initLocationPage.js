var id = document.getElementById("locationId").innerHTML;

function getMessages() {
    $.getJSON(id + "/showMessage", function (messages) {
        let messageDiv = document.getElementById('messages');
        while (messageDiv.firstChild) {
            messageDiv.removeChild(messageDiv.firstChild);
        }
        messages.forEach(function (msg) {
            var div = document.createElement('div');
            div.className = "bs-callout bs-callout-danger";
            var title = document.createElement('h4');
            title.innerHTML = msg[0];
            div.appendChild(title);
            var text = document.createElement('p');
            text.innerHTML = msg[1];
            div.appendChild(text);
            messageDiv.appendChild(div);

        });
    });
}

function indicateWarning() {
    var timerId = setInterval(function() {
        indicateSchemaWarning(maps[0]);
        var tmt = setTimeout(  function () {
            indicateSchemaOk(maps[0]);
        }, 1000);
    }, 2000);
}

function getColors(values){
    var colors = [];
    values.forEach(function (val) {
        if (val) colors.push("red");
        else colors.push("white");
    });
    return colors;
}

function init() {
    var parameters = ['G', 'T'];
    var tAir;
    var sublocations;
    var vals = [];
    var failures;
    /* blocks initialization */
    var rowInDiv = document.getElementById("row-in");
    var rowOutDiv = document.getElementById("row-out");
    var rowPump = document.getElementById("row-pump");
    var rowDelta = document.getElementById("row-delta");
    var rightBlock = document.getElementById("right-block");
    /* constant parameters */

    var delta = [];

    console.log(vals);

    //tAir = 22;

    if (id == 1 ) sublocations = [1, 2];
    else if (id == 2 ) sublocations = [3, 4];
    else if (id == 3)  sublocations = [5, 6];
    else if (id == 4) sublocations = [7, 8];

    $.getJSON(id + "/getValues", function (values) {
        var countParId = 3;
        values.forEach(function (v) {
            if (id == 6 || id == 5){
                vals[0] = v[0];
                return;
            }
            if(v[1] == sublocations[0]){
                if(v[2] == countParId) vals[0] = v[0];
                else vals[1] = v[0];
            }
            else if(v[2] == countParId) vals[2] = v[0];
            else vals[3] = v[0];
        });

        /* parameters initialization */
        tAir = 22;

        if (id == 1 ) sublocations = [1, 2];
        else if (id == 2 ) sublocations = [3, 4];
        else if (id == 3)  sublocations = [5, 6];
        else if (id == 4) sublocations = [7, 8];

        failures = [15,0,15,0];
        delta[0] = Math.round(Math.abs(vals[0]-vals[2])*100)/100;
        delta[1] = Math.round(Math.abs(vals[1]-vals[3])*100)/100;

        // $.getJSON(id + "/getOutsideTemperature", function (t) {
        //     tAir = t[0];
        // });

        /* ***************************************************** */

        var therm1 = document.getElementById("row-in-therm");
        var therm2 = document.getElementById("row-out-therm");
        if (id == 5) {
            insertColdRow(rowInDiv, 9, vals[0]);
            insertColdSchemaRow();
        } else
        if (id == 6) {
            insertPumpRow(rowPump, 1, vals[0]);
            insertPumpSchemaRow();
        }
        else {
            insertRow(rowInDiv, therm1, sublocations[0], vals[0], vals[1]);
                insertRow(rowOutDiv,therm2, sublocations[1], vals[2], vals[3]);
                insertDeltaRow(rowDelta, sublocations, parameters, delta);

        }

    });
    if (id != 6){
        $.getJSON(id + "/getFailureColors", function (colors) {
            if (id == 5) insertColdSchemaRow();
            else insertSchemaRow(2,colors);
        });
    }
}

function initRightBlock(div, sublocations){
    if (id == 5){
        div.insertAdjacentHTML('beforeEnd', '<hr size="15">\n' +
            '                    <p>Построить график:</p>\n' +
            '                    <form>\n' +
            '                        <a href="/graphic/sensor-17">G<sub>9</sub></a>\n' +
            '                    </form>');
    }else if (id == 6){
        div.insertAdjacentHTML('beforeEnd', '<hr size="15">\n' +
            '                    <p>Построить график:</p>\n' +
            '                    <form>\n' +
            '                        <a href="/graphic/sensor-18">P<sub>1</sub></a>\n' +
            '                    </form>');
    }else{
        $.getJSON(id + "/getSensorIds", function (sensors) {
            var senIds = [];
            sensors.forEach(function (s) {
                senIds.push(s);
            });
            div.insertAdjacentHTML('beforeEnd', '<hr size="15">\n' +
                '                    <p>Построить график:</p>\n' +
                '                    <form>\n' +
                '                        <a href="/graphic/sensor-'+senIds[0]+'">G<sub>'+sublocations[0]+'</sub></a>\n' +
                '                        <br>\n' +
                '                        <a href="/graphic/sensor-'+senIds[1]+'">T<sub>'+sublocations[0]+'</sub></a>\n' +
                '                        <br>\n' +
                '                        <a href="/graphic/sensor-'+senIds[2]+'">G<sub>'+sublocations[1]+'</sub></a>\n' +
                '                        <br>\n' +
                '                        <a href="/graphic/sensor-'+senIds[3]+'">T<sub>'+sublocations[1]+'</sub></a>\n' +
                '                    </form>');
        });
    }


}
if (id == 1 ) sublocations = [1, 2];
else if (id == 2 ) sublocations = [3, 4];
else if (id == 3)  sublocations = [5, 6];
else if (id == 4) sublocations = [7, 8];
else sublocations = [1];
var rightBlock = document.getElementById("right-block");
var tAir = 22;

insertThemperature(rightBlock, tAir);
initRightBlock(rightBlock, sublocations);
init();
if (id != 6){
    getMessages();
    window.setInterval(init, 5000);
    window.setInterval(getMessages, 5000);
}
