DROP TABLE IF EXISTS dbo.value;
DROP TABLE IF EXISTS dbo.sensor;
DROP TABLE IF EXISTS dbo.parameter;
DROP TABLE IF EXISTS dbo.location;
DROP TABLE IF EXISTS dbo.sensor_type;
DROP TABLE IF EXISTS dbo.limit;
DROP TABLE IF EXISTS dbo.unit;
DROP TABLE IF EXISTS dbo.message;
DROP TABLE IF EXISTS dbo.authorities;
DROP TABLE IF EXISTS dbo.users;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='sensor' AND xtype='U')
create table sensor
(
	name nvarchar(50) not null,
	sensor_type_id int not null,
	description nvarchar(200),
	id int not null
		constraint sensor_id_pk
			primary key,
	location_id int,
	sublocation int
);

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='sensor_type' AND xtype='U')
create table sensor_type
(
	name nvarchar(100) not null,
	id int not null
		constraint sensor_type_id_pk
			primary key
);

alter table sensor
	add constraint sensor_sensor_type_id_fk
		foreign key (sensor_type_id) references sensor_type;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='parameter' AND xtype='U')
create table parameter
(
	name nvarchar(50) not null,
	unit_id int not null,
	id int not null
		constraint parameter_id_pk
			primary key,
	limit_id int
);

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='unit' AND xtype='U')
create table unit
(
	name nvarchar(50),
	id int not null
		constraint unit_id_pk
			primary key
);

alter table parameter
	add constraint parameter_unit_id_fk
		foreign key (unit_id) references unit;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='value' AND xtype='U')
create table value
(
	id int IDENTITY(1, 1)
		primary key,
	time datetime2 not null,
	value float not null,
	sensor_id int not null
		constraint value_sensor_id_fk
			references sensor,
	parameter_id int not null
		constraint value_parameter_id_fk
			references parameter
);

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='limit' AND xtype='U')
create table limit
(
	id int not null
		primary key,
	min_value float not null,
	max_value float,
	message_id int
);

alter table parameter
	add constraint parameter_limit_id_fk
		foreign key (limit_id) references limit;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='location' AND xtype='U')
create table location
(
	id int not null
		primary key,
	name nvarchar(50) not null
);

alter table sensor
	add constraint sensor_location_id_fk
		foreign key (location_id) references location;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='message' AND xtype='U')
create table message
(
	id int not null
		primary key,
	title nvarchar(200) not null,
	text nvarchar(500) not null
);

alter table limit
	add constraint limit_message_id_fk
		foreign key (message_id) references message;

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='user' AND xtype='U')
CREATE TABLE dbo.users(
	username VARCHAR(30) NOT NULL PRIMARY KEY ,
	password VARCHAR(60) NOT NULL ,
	enabled BIT NOT NULL
);

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='authorities' AND xtype='U')
CREATE TABLE dbo.authorities(
	username VARCHAR(30) NOT NULL ,
	authority VARCHAR(30) NOT NULL ,
	PRIMARY KEY (username, authority),
	CONSTRAINT fk_authorities_users FOREIGN KEY (username) REFERENCES users(username)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT unique_authorities UNIQUE (username, authority)
);